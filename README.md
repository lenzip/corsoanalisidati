# Data Analysis in Particle Physics - UniFi 

This repository contains jupyter notebooks for the Data Analysis in Particle Physics course of the University of Florence.

The repository comes with a [`docker`](https://www.docker.com/) image. You can think of a `docker` image as the installation media of an operating system, only it is the operating system of a machine with exactly what you need installed in it. For this course we have created  a `docker` image that has ROOT, python, jupyter... and the notebooks in this repository. The `docker` image has a name, which in this case is:
```
gitlab-registry.cern.ch/lenzip/corsoanalisidati:latest
```
To use this image you need to follow a few installation steps below.

## Installation

### Get docker desktop

#### For linux:

You need to install `docker engine` following the instructions [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).


#### For Mac:
To get the `docker engine` on Mac you need to install `docker desktop` following instructions [here](https://docs.docker.com/desktop/install/mac-install/).

#### For Windows:

To get the `docker engine` on Windows you need to install `docker desktop` following instructions [here](https://docs.docker.com/desktop/install/windows-install/)

## Quickstart

From a terminal on your computer, run:

```
docker run -p 127.0.0.1:8888:8888 gitlab-registry.cern.ch/lenzip/corsoanalisidati:latest

```
This command will get the image and "install" it. A running image is called a `docker container`, and is, for all intents and purposes, like a virtual machine running on your physical machine, but completely isolated from it. We will need to talk to the `docker container`, and we'll do so via the browser of your physical machine: that is why we need to bind a port from the `docker container` to a port on the local machine: this is what the `-p 127.0.0.1:8888:8888` option achieves. 

Following the command you should get an output like the following:

```
...
...
[I 2024-05-10 13:57:21.163 ServerApp]     http://127.0.0.1:8888/lab
...
...
```
Now all you need to do is copy&paste [`http://127.0.0.1:8888/lab`](http://127.0.0.1:8888/lab) in your web browser and you should have access to the jupyter lab instance and be able to run all the notebooks.

## Important notice on container lifetime and where your work lives
When you switch off your laptop the container survives, but you need to "reactivate" it. This involves a few steps:

1. You need to discover its name. To do this, issue:
    ```
    docker ps -a
    ``` 
    which should result in something like:
    ```
    (base) lenzip@lenzip-XPS-13-9360:~$ docker ps -a
    CONTAINER ID   IMAGE                                                              COMMAND                  CREATED          STATUS                      PORTS     NAMES
    70e6ed44e130   gitlab-registry.cern.ch/lenzip/corsoanalisidati:latest             "/bin/bash -l -c 'ju…"   38 seconds ago   Exited (0) 10 seconds ago             elastic_wilson
    ```
    In this case the name of the container is `elastic_wilson` (docker gives funny names to the containers). You can also decide the name of the container yourself if you add to your `docker run` command an option like `--name [your chosen name]`.
2. Start the container with:
    ```
    docker start [container_name]
    ```
3. (optional) if you want to see the output on the container, attach your terminal to it with
    ```
    docker attach [container_name]
    ```
**IMPORTANT: It may be a good idea to keep your work on your local machine instead that in the container. To do so you can bind mount a local directory (e.g. `mywork`) to the container adding the option `-v mywork:/app/mywork` to your `docker run` command. For this to work, the `mywork` directory needs to be world writeable, which can be achievend, e.g. on linux, with `chmod -R 777 mywork`.**


## Docker cheatsheet

* See running containers: `docker ps`.
* See running and stopped containers `docker ps -a`.
* List images: `docker images`.
* Remove a container: `docker rm [container_name]`. This removes any files you may have saved in the container, it is like wiping the drive. You need to stop the container first (`docker stop [container_name]`).
* Update an image (e.g. if the teacher makes an update to the image): `docker pull gitlab-registry.cern.ch/lenzip/corsoanalisidati:latest`.
* Remove an image: `dorcker rmi [image_id]`.
