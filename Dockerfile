FROM gitlab-registry.cern.ch/cms-cloud/root-vnc:root6.26.10-python3.10 

WORKDIR /app

COPY . /app

USER root

RUN apt-get update && apt-get install -y curl \
    && apt-get install -y vim \
    && rm -rf /var/lib/apt/lists/* \
    && pip install jupyterlab

RUN chmod -R 777 /app


EXPOSE 8888

user cmsusr

CMD ["jupyter lab --ip 0.0.0.0 --port 8888 --no-browser --NotebookApp.password='' --NotebookApp.token=''"]
    
